import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log('newConference: ', newConference);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_attendees: '',
                max_presentations: '',
                location: '',
            }
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        event.preventDefault();
        const value = event.target.value;
        this.setState({name: value});
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value});
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value});
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({max_presentations: value});
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({max_attendees: value});
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                this.setState({locations: data.locations});
            } else {
                throw new Error('Response not ok');
            }
        } catch (e) {
            console.error('ERROR: ', e);
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"></input>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleStartsChange} value={this.state.starts} placeholder="starts" required type="date" id="starts" name="starts" className="form-control"></input>
                        <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEndsChange} value={this.state.ends} placeholder="ends" required type="date" id="ends" name="ends" className="form-control"></input>
                        <label htmlFor="ends">Ends</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description">Description</label>
                        <textarea onChange={this.handleDescriptionChange} value={this.state.description} placeholder="" required id="description" name="description" className="form-control"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxPresentationsChange} value={this.state.max_presentations} placeholder="Max presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"></input>
                        <label htmlFor="max_presentations">Max presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxAttendeesChange} value={this.state.max_attendees} placeholder="Max attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"></input>
                        <label htmlFor="max_attendees">Max attendees</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                        <option value="">Choose a location</option>
                        {this.state.locations.map(location => {
                            return (
                                <option value={location.id} key={location.id}>
                                    {location.name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>  
            </div>
        );
    }
}

export default ConferenceForm;
