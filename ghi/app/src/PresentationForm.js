import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: [],
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        delete data.conference;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log('newPresentation: ', newPresentation);
            const cleared = {
                presenter_name: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            }
            this.setState(cleared);
        }
    }

    handlePresenterNameChange(event) {
        const value = event.target.value;
        this.setState({presenter_name: value});
    }

    handlePresenterEmailChange(event) {
        const value = event.target.value;
        this.setState({presenter_email: value});
    }

    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({company_name: value});
    }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value});
    }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({synopsis: value});
    }
    
    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                this.setState({conferences: data.conferences});
            } else {
                throw new Error('Response not ok');
            }
        } catch (e) {
            console.error('ERROR: ', e);
        }
    }

    render () {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePresenterNameChange} value={this.state.presenter_name} placeholder="Presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control"></input>
                    <label htmlFor="presenter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePresenterEmailChange} value={this.state.presenter_email} placeholder="starts" required type="text" id="presenter_email" name="presenter_email" className="form-control"></input>
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCompanyNameChange} value={this.state.company_name} placeholder="Company name" required type="text" id="company_name" name="company_name" className="form-control"></input>
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" id="title" name="title" className="form-control"></input>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} placeholder="" required id="synopsis" name="synopsis" className="form-control"></textarea>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleConferenceChange} value={this.state.conference} id="conference" name="conference" className="form-select">
                      <option value="">Choose a conference</option>
                      {this.state.conferences.map(conference => {
                        return (
                            <option value={conference.id} key={conference.id}>{conference.name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default PresentationForm;