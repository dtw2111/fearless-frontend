function createCard(conference) {
    return `
    <div class="row m-1">
        <div class="card shadow m-1 p-1 bg-body rounded">
            <img src="${conference.location.picture_url}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title fw-bold">${conference.name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${conference.location.name}</h6>
            <p class="card-text">${conference.description}</p>
            </div>
            <div class="card-footer">${new Date(conference.starts).toLocaleDateString()} - ${new Date(conference.ends).toLocaleDateString()}</div>
        </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // figure out what to do when response is bad
            throw new Error('Response not ok');
        } else {

        const data = await response.json();
        let colNumber = 1;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const conference = details.conference;
                const html = createCard(conference);
                let columnId = 'col-' + String(colNumber);
                let column = document.getElementById(columnId);
                column.innerHTML += html;
                colNumber++;
                if (colNumber === 4) {
                    colNumber = 1;
                }
                }
            }
        }
    } catch (e) {
        // Figure out what to do if an error is raised
        console.error('ERROR: ', e);
    }
    
});
