window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    // get conference data
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.id;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    } else {
        throw new Error("Response not ok");
    }

    //submit form data
    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const formObject = Object.fromEntries(formData);
        console.log('formObject', formObject);
        const json = JSON.stringify(formObject);
        const id = formObject.conference;
        const presentationUrl = `http://localhost:8000/api/conferences/${id}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log('newPresentation', newPresentation);
        } else {
            throw new Error('Form submission not ok');
        }
    });
});