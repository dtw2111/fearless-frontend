window.addEventListener('DOMContentLoaded', async () => {
    // get location data
    const url = 'http://localhost:8000/api/locations/';
    let locationList;
    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const selectTag = document.getElementById('location');
            locationList = data.locations;
            for (let location of locationList) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
        } else {
            throw new Error('Response not ok');
        }
    } catch (e) {
        console.error('ERROR: ', e);
    }

    //submit form data
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const formObject = Object.fromEntries(formData);
        const json = JSON.stringify(formObject);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log('newConference: ', newConference);
        } else {
            throw new Error('Form submission not ok');
        }
    });
});
