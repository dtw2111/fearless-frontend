// get the cookie out of the cookie store
const payloadCookie = cookieStore.get('jwt_access_payload');
if (payloadCookie) {
    // the cookie value is a JSON-formatted string, so parse it
    let decodedPayload;
    payloadCookie.then(obj => {
        decodedPayload = atob(obj.value);
        const payload = JSON.parse(decodedPayload);
        if (payload.user.perms.includes("events.add_conference")) {
            const conferenceTag = document.getElementById("new-conference");
            conferenceTag.classList.remove("d-none")
        }
        if (payload.user.perms.includes("events.add_location")) {
            const locationTag = document.getElementById("new-location");
            locationTag.classList.remove("d-none");
        }
        if (payload.user.perms.includes("presentations.add_presentation")) {
            const presentationTag = document.getElementById("new-presentation");
            presentationTag.classList.remove("d-none");
        }
    });
}