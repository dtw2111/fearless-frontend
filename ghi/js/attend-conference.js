window.addEventListener("DOMContentLoaded", async () => {
    const selectTag = document.getElementById('conference');
    const loadTag = document.getElementById('loading-conference-spinner');
    // get conference data
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    } else {
        throw new Error("Response not ok");
    }
    loadTag.classList.add('d-none');
    selectTag.classList.remove('d-none');

    // post form data
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(attendeesUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            console.log('newAttendee: ', newAttendee);
            formTag.classList.add('d-none');
            const successTag = document.getElementById('success-message');
            successTag.classList.remove('d-none');
        } else {
            throw new Error('Form submission not ok');
        }
    });
});